package com.example.lojaicts.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.lojaicts.api.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
