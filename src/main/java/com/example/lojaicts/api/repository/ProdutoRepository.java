package com.example.lojaicts.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.lojaicts.api.model.Produto;
import com.example.lojaicts.api.repository.produto.ProdutoRepositoryQuery;

public interface ProdutoRepository extends JpaRepository<Produto, Long>, 	ProdutoRepositoryQuery  {

}
