package com.example.lojaicts.api.repository.produto;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.lojaicts.api.model.Produto;
import com.example.lojaicts.api.repository.filter.ProdutoFilter;

public interface ProdutoRepositoryQuery {
	
	public  Page<Produto> filtrar (ProdutoFilter produtoFilter, Pageable pageable);
	

}
